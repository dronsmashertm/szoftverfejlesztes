﻿using Microsoft.Kinect;
using NAudio.Midi;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace MoveToRhythm
{

    public partial class MainWindow : Window
    {

        String hangszer = "d";

        //elso loop
        DateTime start;
        DateTime end;
        Boolean loop = false;
        Boolean felvetel1 = false;
        int a, b;

        //masodikloop

        DateTime start2;
        DateTime end2;
        Boolean loop2 = false;
        Boolean felvetel2 = false;
        int a2, b2;

        //harmadik loop

        DateTime start3;
        DateTime end3;
        Boolean loop3 = false;
        Boolean felvetel3 = false;
        int a3, b3;

        #region MIDI

        public MidiIn midiIn;
        MidiOut midiOut = new MidiOut(0);

        public string GetMIDIInDevice()
        {
            string returnDevice;
            returnDevice = MidiIn.DeviceInfo(0).ProductName;

            return returnDevice;
        }

        public void StartMonitoring(int MIDIInDevice)
        {
            if (midiIn == null)
            {
                midiIn = new MidiIn(MIDIInDevice);
            }
            midiIn.Start();
        }

        public void midiIn_MessageReceived(object sender, MidiInMessageEventArgs e)
        {
            // Exit if the MidiEvent is null or is the AutoSensing command code  
            if (e.MidiEvent != null && e.MidiEvent.CommandCode == MidiCommandCode.AutoSensing)
            {
                return;
            }
            if (e.MidiEvent.CommandCode == MidiCommandCode.NoteOn)
            {
                NoteEvent ne;
                ne = (NoteEvent)e.MidiEvent;

                if (ne.Velocity > 0)
                {
                    Console.WriteLine(ne.NoteNumber);
                    playMySound(ne.NoteNumber, hangszer);
                    savelog(ne.NoteNumber + hangszer);
                }
            }
        }
        #endregion

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.Key == Key.A)
            {
                playMySound(50, hangszer);
                savelog("50" + hangszer);

            }
            if (e.Key == Key.S)
            {
                playMySound(51, hangszer);
                savelog("51" + hangszer);
            }
            if (e.Key == Key.D)
            {
                playMySound(52, hangszer);
                savelog("52" + hangszer);
            }
            if (e.Key == Key.F)
            {
                playMySound(53, hangszer);
                savelog("53" + hangszer);
            }
            if (e.Key == Key.G)
            {
                playMySound(54, hangszer);
                savelog("54" + hangszer);
            }
            if (e.Key == Key.H)
            {
                playMySound(55, hangszer);
                savelog("55" + hangszer);
            }
            if (e.Key == Key.J)
            {
                playMySound(56, hangszer);
                savelog("56" + hangszer);
            }
            if (e.Key == Key.K)
            {
                playMySound(57, hangszer);
                savelog("57" + hangszer);
            }
            if (e.Key == Key.L)
            {
                playMySound(58, hangszer);
                savelog("58" + hangszer);

            }




            if (e.Key == Key.O)
            {
                playMySound(0,"beep");
                hangszer = "d";
            }
            if (e.Key == Key.I)
            {
                playMySound(0,"beep");
                hangszer = "g";
            }
            if (e.Key == Key.U)
            {
                playMySound(0,"beep");
                hangszer = "p";
            }
            if (e.Key == Key.R)
            {
                loop = !loop;
                testThreadloop1();
            }
            if (e.Key == Key.T)
            {
                loop2 = !loop2;
                testThreadloop2();
            }
            if (e.Key == Key.Z)
            {
                loop3 = !loop3;
                testThreadloop3();
            }
            if (e.Key == Key.Q)
            {
                if (!felvetel1 && !felvetel2 && !felvetel3)
                {
                    felvetel1 = true;
                    FileStream fcreate = File.Open("log1.txt", FileMode.Create);
                    using (StreamWriter writer =
                    new StreamWriter(fcreate))
                    {
                    }
                    start = DateTime.Now;
                    playMySound(0, "beep");
                    a = 0;
                }
                else
                {
                    if (felvetel1)
                    {
                        savelog("--");
                        playMySound(0, "beep");
                        felvetel1 = false;
                    }
                }
            }
            if (e.Key == Key.W)
            {
                if (!felvetel1 && !felvetel2 && !felvetel3)
                {
                    felvetel2 = true;
                    FileStream fcreate = File.Open("log2.txt", FileMode.Create);
                    using (StreamWriter writer =
                    new StreamWriter(fcreate))
                    {
                    }
                    start2 = DateTime.Now;
                    a2 = 0;
                    playMySound(0, "beep");
                }
                else
                {
                    if (felvetel2)
                    {
                        savelog("--");
                        playMySound(0, "beep");
                        felvetel2 = false;
                    }
                }
            }
            if (e.Key == Key.E)
            {
                if (!felvetel1 && !felvetel2 && !felvetel3)
                {
                    felvetel3 = true;
                    FileStream fcreate = File.Open("log3.txt", FileMode.Create);
                    using (StreamWriter writer =
                    new StreamWriter(fcreate))
                    {
                    }
                    start3 = DateTime.Now;
                    a3 = 0;
                    playMySound(0, "beep");
                }
                else
                {
                    if (felvetel3)
                    {
                        savelog("--");
                        playMySound(0, "beep");
                        felvetel3 = false;
                    }
                }
            }
        }
        
        public void testThreadloop1()
        {
            ImageBrush ib = new ImageBrush();
            ib.ImageSource = new BitmapImage(new Uri(@"Images\\pause.png", UriKind.Relative));
            play.Background = ib;
            try
            {
                string[] lines = System.IO.File.ReadAllLines("log1.txt");
                if (!lines[0][0].Equals("-"))
                {
                    new System.Threading.Thread(() =>
                    {
                        while (loop)
                        {
                            looping(lines,1);
                        }
                    }).Start();
                }
            }
            catch (FileNotFoundException e) {
                Debug.WriteLine(e);
                ib = new ImageBrush();
                ib.ImageSource = new BitmapImage(new Uri(@"Images\\play.png", UriKind.Relative));
                play.Background = ib;
            }
}

        public void testThreadloop2()
        {
            
            ImageBrush ib = new ImageBrush();
            ib.ImageSource = new BitmapImage(new Uri(@"Images\\pause.png", UriKind.Relative));
            play2.Background = ib;
            try { 
                string[] lines = System.IO.File.ReadAllLines("log2.txt");
                if (!lines[0][0].Equals("-"))
                {
                    new System.Threading.Thread(() =>
                    {
                        while (loop2)
                        {
                            looping(lines,2);
                        }
                    }).Start();
                }
            }
            catch (FileNotFoundException e) {
                Debug.WriteLine(e);
                ib = new ImageBrush();
                ib.ImageSource = new BitmapImage(new Uri(@"Images\\play.png", UriKind.Relative));
                play2.Background = ib;
            }
        }

        public void testThreadloop3()
        {
            ImageBrush ib = new ImageBrush();
            ib.ImageSource = new BitmapImage(new Uri(@"Images\\pause.png", UriKind.Relative));
            play3.Background = ib;
            try
            {
                string[] lines = System.IO.File.ReadAllLines("log3.txt");
                if (!lines[0][0].Equals("-"))
                {
                    new System.Threading.Thread(() =>
                    {
                        while (loop3)
                        {
                            looping(lines,3);
                        }
                    }).Start();
                }
            }
            catch (FileNotFoundException e) {
                Debug.WriteLine(e);
                ib = new ImageBrush();
                ib.ImageSource = new BitmapImage(new Uri(@"Images\\play.png", UriKind.Relative));
                play3.Background = ib;
            }
        }

        public void looping(string[] lines,int loopnumber)
        {
            if (lines != null && lines.Length > 0)
            {
                if (!lines[0][0].Equals("-"))
                {
                    
                    foreach (string line in lines)
                    {
                        if (loop && loopnumber == 1 || loop2 && loopnumber == 2 || loop3 && loopnumber == 3)
                        {
                            String key = line.Substring(0, 2);
                            int noteNum;
                            int.TryParse(key, out noteNum);
                            String hangloop = line[2].ToString();
                            int milis;
                            String milisec = line.Substring(3);
                            int.TryParse(milisec, out milis);
                            System.Threading.Thread.Sleep(milis);
                            playMySound(noteNum, hangloop);
                        }
                    }
                }
            }
        }

        public void playMySound(int szam, String hang)
        {
            new System.Threading.Thread(() =>
            {
                var player = new MediaPlayer();
                Uri v1 = new Uri(@"sounds\\" + hang + szam + ".wav", UriKind.Relative);
                player.Open(v1);
                player.Play();
                System.Threading.Thread.Sleep(5000);
                player.Close();
            }).Start();
        }

        public void savelog(String key)
        {
            Debug.WriteLine("adat: " + key + " " + felvetel1 + felvetel2 + felvetel3);
            if (felvetel1)
            {
                FileStream fappend = File.Open("log1.txt", FileMode.Append);
                end = DateTime.Now;
                int milliseconds = (int)(end - start).TotalMilliseconds;
                b = milliseconds - a;
                Debug.WriteLine(milliseconds);
                using (StreamWriter writer =
                new StreamWriter(fappend))
                {
                    writer.WriteLine(key + b);
                }
                a = milliseconds;
                fappend.Close();                
            }
            if (felvetel2)
            {
                FileStream fappend = File.Open("log2.txt", FileMode.Append);
                end2 = DateTime.Now;
                int milliseconds = (int)(end2 - start2).TotalMilliseconds;
                b2 = milliseconds - a2;
                Debug.WriteLine(milliseconds);
                using (StreamWriter writer =
                new StreamWriter(fappend))
                {
                    writer.WriteLine(key + b2);
                }
                a2 = milliseconds;
                fappend.Close();
            }
            if (felvetel3)
            {
                FileStream fappend = File.Open("log3.txt", FileMode.Append);
                end3 = DateTime.Now;
                int milliseconds = (int)(end3 - start3).TotalMilliseconds;
                b3 = milliseconds - a3;
                Debug.WriteLine(milliseconds);
                using (StreamWriter writer =
                new StreamWriter(fappend))
                {
                    writer.WriteLine(key + b3);
                }
                a3 = milliseconds;
                fappend.Close();
            }
        }
        
        #region Constructor

        public MainWindow()
        {
            InitializeComponent();
        }

        #endregion

        #region Kinect

        KinectSensor _sensor;
        MultiSourceFrameReader _reader;
        IList<Body> _bodies;

        String allapot = "OPEN";
        String allapot2 = "OPEN";


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                StartMonitoring(0);
                midiIn.MessageReceived += new EventHandler<MidiInMessageEventArgs>(midiIn_MessageReceived);
            }
            catch (NAudio.MmException exception)
            {

                //TODO csinálni valamit exceptiont ablakban
            }

            _sensor = KinectSensor.GetDefault();

            if (_sensor != null)
            {
                _sensor.Open();

                _reader = _sensor.OpenMultiSourceFrameReader(FrameSourceTypes.Color | FrameSourceTypes.Depth | FrameSourceTypes.Infrared | FrameSourceTypes.Body);
                _reader.MultiSourceFrameArrived += Reader_MultiSourceFrameArrived;
            }

        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (_reader != null)
            {
                _reader.Dispose();
            }

            if (_sensor != null)
            {
                _sensor.Close();
            }
            Environment.Exit(Environment.ExitCode);
        }

        void Reader_MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e)
        {
            
                var reference = e.FrameReference.AcquireFrame();

                // Color
                using (var frame = reference.ColorFrameReference.AcquireFrame())
                {
                    if (frame != null)
                    {
                        camera.Source = frame.ToBitmap();
                    }
                }

                // Body
                using (var frame = reference.BodyFrameReference.AcquireFrame())
                {
                    if (frame != null)
                    {
                    kinect.Children.Clear();

                        _bodies = new Body[frame.BodyFrameSource.BodyCount];

                        frame.GetAndRefreshBodyData(_bodies);

                    foreach (var body in _bodies)
                    {
                        if (body != null)
                        {
                            if (body.IsTracked)
                            {
                                // Find the joints
                                Joint handRight = body.Joints[JointType.HandRight];
                                Joint handLeft = body.Joints[JointType.HandLeft];
                                kinect.DrawHand(handRight, _sensor.CoordinateMapper);
                                kinect.DrawHand(handLeft, _sensor.CoordinateMapper);

                                // Find the hand states
                                string rightHandState = "-";
                                string leftHandState = "-";
                                float x;
                                float y;
                                float left_x;
                                float left_y;

                                if (body.HandRightState == HandState.Open)
                                {
                                    x = handRight.Position.X / handRight.Position.Z;
                                    y = handRight.Position.Y / handRight.Position.Z;

                                    allapot = "OPEN";
                                }

                                if (body.HandLeftState == HandState.Open)
                                {
                                    left_x = handLeft.Position.X / handLeft.Position.Z;
                                    left_y = handLeft.Position.Y / handLeft.Position.Z;

                                    allapot2 = "OPEN";
                                }


                                if (body.HandLeftState == HandState.Closed)
                                {
                                    left_x = handLeft.Position.X / handLeft.Position.Z;
                                    left_y = handLeft.Position.Y / handLeft.Position.Z;

                                    if (allapot2.Equals("OPEN"))
                                    {

                                        if (left_x < -0.45 && left_x > -0.7 && left_y > 0.25 && left_y < 0.5)
                                        {
                                            hangszer = "g";
                                            playMySound(0, "beep");
                                        }

                                        if (left_x > -0.235 && left_x < 0 && left_y > 0.25 && left_y < 0.5)
                                        {
                                            hangszer = "p";
                                            playMySound(0, "beep");
                                        }

                                        if (left_x > 0.250 && left_x < 0.5 && left_y > 0.25 && left_y < 0.5)
                                        {
                                            hangszer = "d";
                                            playMySound(0, "beep");
                                        }

                                        // loop 1
                                        if (left_x < -0.45 && left_x > -0.7 && left_y > -0.35 && left_y < -0.1)
                                        {
                                            startStopFelvetel1();
                                        }
                                        if (left_x < -0.45 && left_x > -0.7 && left_y > -0.35 && left_y < 0.225)
                                        {
                                            startStopLoop(1);

                                        }

                                        //loop 2

                                        if (left_x > -0.235 && left_x < 0 && left_y > -0.35 && left_y < -0.1)
                                        {
                                            startStopFelvetel2();
                                        }

                                        if (left_x > -0.235 && left_x < 0 && left_y > -0.35 && left_y < 0.225)
                                        {
                                            startStopLoop(2);
                                        }

                                        // loop 3

                                        if (left_x > 0.250 && left_x < 0.5 && left_y > -0.35 && left_y < -0.1)
                                        {
                                            startStopFelvetel3();
                                        }
                                        if (left_x > 0.250 && left_x < 0.5 && left_y > -0.35 && left_y < 0.225)
                                        {
                                            startStopLoop(3);
                                        }

                                    }
                                    allapot2 = "CLOSED";
                                }
                                    

                                    if (body.HandRightState == HandState.Closed)
                                    {
                                        x = handRight.Position.X / handRight.Position.Z;
                                        y = handRight.Position.Y / handRight.Position.Z;


                                        if (allapot.Equals("OPEN"))
                                        {

                                            if (x < -0.45 && x > -0.7 && y > 0.25 && y < 0.5)
                                            {
                                                hangszer = "g";
                                                playMySound(0, "beep");
                                            }

                                            if (x > -0.235 && x < 0 && y > 0.25 && y < 0.5)
                                            {
                                                hangszer = "p";
                                                playMySound(0, "beep");
                                            }

                                            if (x > 0.250 && x < 0.5 && y > 0.25 && y < 0.5)
                                            {
                                                hangszer = "d";
                                                playMySound(0, "beep");
                                            }

                                            // loop 1
                                            if (x < -0.45 && x > -0.7 && y > -0.35 && y < -0.1)
                                            {
                                                startStopFelvetel1();
                                            }
                                            if (x < -0.45 && x > -0.7 && y > -0.35 && y < 0.225)
                                            {
                                                startStopLoop(1);

                                            }

                                            //loop 2

                                            if (x > -0.235 && x < 0 && y > -0.35 && y < -0.1)
                                            {
                                                startStopFelvetel2();
                                            }

                                            if (x > -0.235 && x < 0 && y > -0.35 && y < 0.225)
                                            {
                                                startStopLoop(2);

                                            }

                                            // loop 3

                                            if (x > 0.250 && x < 0.5 && y > -0.35 && y < -0.1)
                                            {
                                                startStopFelvetel3();
                                            }
                                            if (x > 0.250 && x < 0.5 && y > -0.35 && y < 0.225)
                                            {
                                                startStopLoop(3);

                                            }
                                        }
                                        allapot = "CLOSED";
                                    }

                                















                                    /*


                                if (false)
                                {
                                    switch (body.HandRightState)
                                    {
                                        case HandState.Open:
                                            x = handRight.Position.X / handRight.Position.Z;
                                            y = handRight.Position.Y / handRight.Position.Z;

                                            allapot = "OPEN";
                                            Point pont = handRight.Scale(_sensor.CoordinateMapper);
                                            Debug.WriteLine("R+O" + x.ToString("n3") + " " + y.ToString("n3") + "  point " + pont.X + ", " + pont.Y);
                                            break;
                                        case HandState.Closed:
                                            x = handRight.Position.X / handRight.Position.Z;
                                            y = handRight.Position.Y / handRight.Position.Z;
                                            pont = handRight.Scale(_sensor.CoordinateMapper);
                                            Debug.WriteLine("R+C" + x.ToString("n3") + " " + y.ToString("n3") + "  point " + pont.X + ", " + pont.Y);
                                            //Debug.WriteLine("R+C" + x.ToString("n3") + " " + y.ToString("n3"));
                                            if (allapot.Equals("OPEN"))
                                            {

                                                if (x < -0.45 && x > -0.7 && y > 0.25 && y < 0.5)
                                                {
                                                    hangszer = "g";
                                                    playMySound(0, "beep");
                                                }

                                                if (x > -0.235 && x < 0 && y > 0.25 && y < 0.5)
                                                {
                                                    hangszer = "p";
                                                    playMySound(0, "beep");
                                                }

                                                if (x > 0.250 && x < 0.5 && y > 0.25 && y < 0.5)
                                                {
                                                    hangszer = "d";
                                                    playMySound(0, "beep");
                                                }

                                                // loop 1
                                                if (x < -0.45 && x > -0.7 && y > -0.35 && y < -0.1)
                                                {
                                                    startStopFelvetel1();
                                                }
                                                if (x < -0.45 && x > -0.7 && y > -0.35 && y < 0.225)
                                                {
                                                    startStopLoop(1);

                                                }

                                                //loop 2

                                                if (x > -0.235 && x < 0 && y > -0.35 && y < -0.1)
                                                {
                                                    startStopFelvetel2();
                                                }

                                                if (x > -0.235 && x < 0 && y > -0.35 && y < 0.225)
                                                {
                                                    startStopLoop(2);

                                                }

                                                // loop 3

                                                if (x > 0.250 && x < 0.5 && y > -0.35 && y < -0.1)
                                                {
                                                    startStopFelvetel3();
                                                }
                                                if (x > 0.250 && x < 0.5 && y > -0.35 && y < 0.225)
                                                {
                                                    startStopLoop(3);

                                                }
                                            }
                                            allapot = "CLOSED";
                                            break;
                                        case HandState.Lasso:
                                            x = handRight.Position.X / handRight.Position.Z;
                                            y = handRight.Position.Y / handRight.Position.Z;
                                            break;
                                        case HandState.Unknown:
                                            rightHandState = "Unknown";
                                            break;
                                        case HandState.NotTracked:
                                            rightHandState = "Not tracked";
                                            break;
                                        default:
                                            break;
                                    }

                                    switch (body.HandLeftState)
                                    {
                                        case HandState.Open:
                                            allapot2 = "OPEN";
                                            left_x = handLeft.Position.X / handLeft.Position.Z;
                                            left_y = handLeft.Position.Y / handLeft.Position.Z;
                                            Point pont = handRight.Scale(_sensor.CoordinateMapper);
                                            Debug.WriteLine("R+O" + left_x.ToString("n3") + " " + left_y.ToString("n3") + "  point " + pont.X + ", " + pont.Y);
                                            Debug.WriteLine("L+O" + left_x.ToString("n3") + " " + left_y.ToString("n3"));
                                            break;
                                        case HandState.Closed:
                                            left_x = handLeft.Position.X * handLeft.Position.Z;
                                            left_y = handLeft.Position.Y * handLeft.Position.Z;
                                            pont = handRight.Scale(_sensor.CoordinateMapper);
                                            Debug.WriteLine("R+O" + left_x.ToString("n3") + " " + left_y.ToString("n3") + "  point " + pont.X + ", " + pont.Y);
                                            Debug.WriteLine("L+C" + left_x.ToString("n3") + " " + left_y.ToString("n3"));

                                            if (allapot2.Equals("OPEN"))
                                            {

                                                if (left_x < -0.45 && left_x > -0.7 && left_y > 0.25 && left_y < 0.5)
                                                {
                                                    hangszer = "g";
                                                    playMySound(0, "beep");
                                                }

                                                if (left_x > -0.235 && left_x < 0 && left_y > 0.25 && left_y < 0.5)
                                                {
                                                    hangszer = "p";
                                                    playMySound(0, "beep");
                                                }

                                                if (left_x > 0.250 && left_x < 0.5 && left_y > 0.25 && left_y < 0.5)
                                                {
                                                    hangszer = "d";
                                                    playMySound(0, "beep");
                                                }

                                                // loop 1
                                                if (left_x < -0.45 && left_x > -0.7 && left_y > -0.35 && left_y < -0.1)
                                                {
                                                    startStopFelvetel1();
                                                }
                                                if (left_x < -0.45 && left_x > -0.7 && left_y > -0.35 && left_y < 0.225)
                                                {
                                                    startStopLoop(1);

                                                }

                                                //loop 2

                                                if (left_x > -0.235 && left_x < 0 && left_y > -0.35 && left_y < -0.1)
                                                {
                                                    startStopFelvetel2();
                                                }

                                                if (left_x > -0.235 && left_x < 0 && left_y > -0.35 && left_y < 0.225)
                                                {
                                                    startStopLoop(2);
                                                }

                                                // loop 3

                                                if (left_x > 0.250 && left_x < 0.5 && left_y > -0.35 && left_y < -0.1)
                                                {
                                                    startStopFelvetel3();
                                                }
                                                if (left_x > 0.250 && left_x < 0.5 && left_y > -0.35 && left_y < 0.225)
                                                {
                                                    startStopLoop(3);
                                                }
                                            }

                                            allapot2 = "CLOSED";
                                            break;
                                        case HandState.Lasso:
                                            left_x = handLeft.Position.X * handLeft.Position.Z;
                                            left_y = handLeft.Position.Y * handLeft.Position.Z;
                                            Debug.WriteLine("L+L" + left_x.ToString("n3") + " " + left_y.ToString("n3"));
                                            //leftHandState = "x: " + handLeft.Position.X.ToString("n2") + " y: " + handLeft.Position.Y.ToString("n2") + " z: " + handLeft.Position.X.ToString("n2");

                                            break;
                                        case HandState.Unknown:
                                            left_x = handLeft.Position.X * handLeft.Position.Z;
                                            left_y = handLeft.Position.Y * handLeft.Position.Z;
                                            //leftHandState = "x: " + handLeft.Position.X.ToString("n2") + " y: " + handLeft.Position.Y.ToString("n2") + " z: " + handLeft.Position.X.ToString("n2");

                                            break;
                                        case HandState.NotTracked:
                                            //leftHandState = "Not tracked";
                                            break;
                                        default:
                                            break;
                                    }
                                }*/
                            }
                        }
                    }
                    }
                }
            
        }

        public void startStopLoop(int melyik)
        {
            switch (melyik)
            {

                case 1:
                    if (!felvetel1)
                    {
                        loop = !loop;
                        if (loop)
                        {
                            ImageBrush ib = new ImageBrush();
                            ib.ImageSource = new BitmapImage(new Uri(@"Images\\pause.png", UriKind.Relative));
                            play.Background = ib;
                            playMySound(0,"beep");
                            testThreadloop1();
                        }else
                        {
                            ImageBrush ib = new ImageBrush();
                            ib.ImageSource = new BitmapImage(new Uri(@"Images\\play.png", UriKind.Relative));
                            play.Background = ib;
                        }
                    }
                    break;
                case 2:

                    if (!felvetel2)
                    {
                        loop2 = !loop2;
                        if (loop2)
                        {
                            ImageBrush ib = new ImageBrush();
                            ib.ImageSource = new BitmapImage(new Uri(@"Images\\pause.png", UriKind.Relative));
                            play2.Background = ib;
                            playMySound(0,"beep");
                            testThreadloop2();
                        }else
                        {
                            ImageBrush ib = new ImageBrush();
                            ib.ImageSource = new BitmapImage(new Uri(@"Images\\play.png", UriKind.Relative));
                            play2.Background = ib;
                        }
                    }
                    break;
                case 3:
                    if (!felvetel3)
                    {
                        loop3 = !loop3;
                        if (loop3)
                        {
                            ImageBrush ib = new ImageBrush();
                            ib.ImageSource = new BitmapImage(new Uri(@"Images\\pause.png", UriKind.Relative));
                            play3.Background = ib;
                            playMySound(0,"beep");
                            testThreadloop3();
                        }else
                        {
                            ImageBrush ib = new ImageBrush();
                            ib.ImageSource = new BitmapImage(new Uri(@"Images\\play.png", UriKind.Relative));
                            play3.Background = ib;
                        }
                    }
                    break;
                default:
                    break;
            }

        }
        
        public void startStopFelvetel1()
        {
            if (!loop)
            {
                if (!felvetel1 && !felvetel2 && !felvetel3)
                {
                    felvetel1 = true;
                    playMySound(0,"beep");
                    FileStream fcreate = File.Open("log1.txt", FileMode.Create);
                    using (StreamWriter writer =
                    new StreamWriter(fcreate))
                    {
                    }
                    start = DateTime.Now;
                    a = 0;
                    ImageBrush ib = new ImageBrush();
                    ib.ImageSource = new BitmapImage(new Uri(@"Images\\buttongr.png", UriKind.Relative));
                    Button1.Background = ib;
                }
                else
                {
                    if (felvetel1)
                    {
                        savelog("--");
                        playMySound(0,"beep");
                        felvetel1 = false;
                        loop = true;
                        ImageBrush ib = new ImageBrush();
                        ib.ImageSource = new BitmapImage(new Uri(@"Images\\buttonr.png", UriKind.Relative));
                        Button1.Background = ib;
                        testThreadloop1();
                    }
                }
            }
        }

        public void startStopFelvetel2()
        {
            if (!loop2)
            {
                if (!felvetel1 && !felvetel2 && !felvetel3)
                {
                    felvetel2 = true;
                    playMySound(0,"beep");
                    FileStream fcreate = File.Open("log2.txt", FileMode.Create);
                    using (StreamWriter writer =
                    new StreamWriter(fcreate))
                    {
                    }
                    start2 = DateTime.Now;
                    a2 = 0;
                    ImageBrush ib = new ImageBrush();
                    ib.ImageSource = new BitmapImage(new Uri(@"Images\\buttongr.png", UriKind.Relative));
                    Button2.Background = ib;
                }
                else
                {
                    if (felvetel2)
                    {
                        savelog("--");
                        playMySound(0,"beep");
                        felvetel2 = false;
                        ImageBrush ib = new ImageBrush();
                        ib.ImageSource = new BitmapImage(new Uri(@"Images\\buttonr.png", UriKind.Relative));
                        Button2.Background = ib;
                        loop2 = true;
                        testThreadloop2();
                    }
                }
            }


        }

        public void startStopFelvetel3()
        {
            if (!loop3)
            {
                if (!felvetel1 && !felvetel2 && !felvetel3)
                {
                    felvetel3 = true;
                    playMySound(0,"beep");
                    FileStream fcreate = File.Open("log3.txt", FileMode.Create);
                    using (StreamWriter writer =
                    new StreamWriter(fcreate))
                    {
                    }
                    start3 = DateTime.Now;
                    a3 = 0;
                    ImageBrush ib = new ImageBrush();
                    ib.ImageSource = new BitmapImage(new Uri(@"Images\\buttongr.png", UriKind.Relative));
                    Button3.Background = ib;
                }
                else
                {
                    if (felvetel3)
                    {
                        savelog("--");
                        playMySound(0,"beep");
                        felvetel3 = false;
                        ImageBrush ib = new ImageBrush();
                        ib.ImageSource = new BitmapImage(new Uri(@"Images\\buttonr.png", UriKind.Relative));
                        Button3.Background = ib;
                        loop3 = true;
                        testThreadloop3();
                    }
                }
            }


        }

        #endregion

    }
}
